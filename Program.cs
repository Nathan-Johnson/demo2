﻿using System;

namespace ConsoleApplication
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Console.Clear();
            Console.WriteLine("Today is the 8th of March 2017");
            Console.WriteLine("");
            Console.ReadKey();
            Console.Clear();
            Console.WriteLine("Press <enter> to exit");
            Console.ReadKey();
            Console.Clear();
        }
    }
}
